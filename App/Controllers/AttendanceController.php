<?php


class AttendanceController
{

    private $db ;
    private $table = "attendance" ;
    private $fillable = ['employee_id' , 'atten_month' ,'login', 'logout'];
    public function __construct()
    {
        $this->db = new DB();
        $this->db->table($this->table);
        $this->db->fillable = $this->fillable;


    }

    public function index(){
        Middleware::checkAuthenticationAdmin();
        $data['title'] = "HR System";
        $data['active'] = 'Attendance';
        $data['attendance'] = $this->db->getWithJoin('employees' ,'employee_id' ,'id');
        require __DIR__ . '/./../template/attendance.php';
    }
    public function import(){
        Middleware::checkAuthenticationAdmin();
        $data['title'] = "HR System";
        $data['active'] = 'Attendance';
        require __DIR__ . '/./../template/attendance-import.php';

    }

    public function importSubmit(){
        Middleware::checkAuthenticationAdmin();
        $fileTemp = $_FILES['sheet']['tmp_name'];
        $fileContent = array_map('str_getcsv' , file($fileTemp));
        array_shift($fileContent);


        foreach ($fileContent as $row){
            $arr = [
                'employee_id' => $row[0],
                'atten_month' => $_POST['atten_month'],
                'login' => date("Y-m-d H:i:s", strtotime($row[1])),
                'logout' => date("Y-m-d H:i:s", strtotime($row[2])),
            ];
            $this->db->insert($this->fillable ,$arr);
        }

        return redirect('attendance');
    }

    public function myAttendance(){
        Middleware::checkAuthenticationUser();
        $data['title'] = "HR System";
        $data['active'] = 'Attendance';
        $user = Auth::user();
        $data['attendance'] = $this->db->where('employee_id' , $user['id'])->get();
        require __DIR__ . '/./../template/attendance-emp.php';

    }
}