<?php
/**
 * Created by PhpStorm.
 * User: engha
 * Date: 5/2/2019
 * Time: 6:09 PM
 */

class AuthController
{

    public function login()
    {
        Middleware::redirectIfAuthentication();
        $data['title'] = "HR System";
        require __DIR__ . './../template/login.php';
    }

    public function dologin()
    {
        Middleware::redirectIfAuthentication();
        $email = $_POST['email'];
        $password = $_POST['password'];
        $validate = new Vaildatetor();
        $validate->make($email)->name('email')->required()->email();
        $validate->make($password)->name('password')->required();
        if ($validate->fails()) {
            return redirect('login');
        }
        if (Auth::attempt([$email, $password])) {
            return redirect('');
        } else if (Auth::attempt([$email ,$password],'employees')) {
            return redirect('');
        }else{
            Sessions::flash('errors', ['Invalid Email or Password']);
            return redirect('login');
        }
    }

    public function logout()
    {
        Middleware::checkAuthentication();
        Auth::logout();
        return redirect('login');
    }
}