<?php
/**
 * Created by PhpStorm.
 * User: hassan
 * Date: 5/1/2019
 * Time: 2:16 PM
 */

class DepartmentsController
{

    private $db ;
    private $table = "departments" ;
    private $fillable = ['title' , 'description'];
    public function __construct()
    {
        $this->db = new DB();
        $this->db->table($this->table);
        $this->db->fillable = $this->fillable;


    }

    public function index(){
        Middleware::checkAuthenticationAdmin();


        $data['title'] = "HR System";
        $data['active'] = 'Departments';
        $data['departments'] = $this->db->get() ;
        require __DIR__ . './../template/departments.php';
    }
    public function add(){
        Middleware::checkAuthenticationAdmin();

        $data['title'] = "Add Department";
        $data['active'] = 'Departments';

        require __DIR__ . './../template/add-department.php';
    }

    public function store(){
        Middleware::checkAuthenticationAdmin();

        $inputs['title'] = $_POST['title'];
        $inputs['description'] = $_POST['description'];
        $validate = new Vaildatetor();
        $validate->make($inputs['title'])->name('title')->required();
        $validate->make($inputs['description'])->name('description')->required();
        if ($validate->fails()){
            return  redirect('departments/add');
        }
        if ($this->db->insert($this->fillable , $inputs)){
            return  redirect('departments');

        }
    }

    public function edit($id){
        Middleware::checkAuthenticationAdmin();

        $data['title'] = "Edit Department";
        $data['active'] = 'Departments';
        $data['department'] = $this->db->where('id' , $id)->getOrFail();
        require __DIR__ . './../template/edit-department.php';
    }
    public function update($id){
        Middleware::checkAuthenticationAdmin();

        $inputs['title'] = $_POST['title'];
        $inputs['description'] = $_POST['description'];

        $validate = new Vaildatetor();
        $validate->make($inputs['title'])->name('title')->required();
        $validate->make($inputs['description'])->name('description')->required();

        if ($validate->fails()){
            return redirect('departments/edit'.$id);
        }

        if ($this->db->where('id' , $id)->update($this->fillable , $inputs)){
            redirect('departments');
        }
    }
    public function delete($id){
        Middleware::checkAuthenticationAdmin();

        if ($this->db->where('id', $id)->delete()){
            redirect('departments');
        }
    }

}