<?php


class homeController
{

    private $db ;
    public function __construct()
    {
        $this->db = new DB();
    }
    public function index()
    {

        Middleware::checkAuthentication();
        $data['title'] = "HR System";
        $data['active'] = 'Home';

        $data['counts']['employees'] = !empty($this->db->table('employees')->get()) ? count($this->db->table('employees')->get()) : "0";
        $data['counts']['departments'] = !empty($this->db->table('departments')->get()) ?  count($this->db->table('departments')->get()) : "0";
        $data['counts']['attendance'] = !empty($this->db->table('attendance')->get()) ? count($this->db->table('attendance')->get()) : "0";
        $data['counts']['leave_requests'] = !empty($this->db->table('leave_requests')->get()) ? count($this->db->table('leave_requests')->get()) : "0";

        $data['latest'] = $this->db->table('leave_requests')->fillable(['employee_id' , 'type' ,'description', 'date_from' , 'date_to' , 'status'])->limit(5)->getWithJoin('employees' ,'employee_id' ,'id');
        $data['my_requests'] = $this->db->table('leave_requests')->where('employee_id' , Auth::user()['id'])->get();
        require __DIR__ . './../template/main.php';
    }

    public function login()
    {
        Middleware::redirectIfAuthentication();
        $data['title'] = "HR System";
        require __DIR__ . './../template/login.php';
    }

    public function dologin()
    {
        Middleware::redirectIfAuthentication();
        $email = $_POST['email'];
        $password = $_POST['password'];
        $validate = new Vaildatetor();
        $validate->make($email)->name('email')->required()->email();
        $validate->make($password)->name('password')->required();
        if ($validate->fails()) {
            return redirect('login');
        }
        if (Auth::attempt([$email, $password])) {
            return redirect('');
        } else if (Auth::attempt([$email ,$password],'employees')) {
            return redirect('');
        }else{
            Sessions::flash('errors', ['Invalid Email or Password']);
            return redirect('login');
        }
    }

    public function logout()
    {
        Middleware::checkAuthentication();
        Auth::logout();
        return redirect('login');
    }
}