<?php
/**
 * Created by PhpStorm.
 * User: hassan
 * Date: 5/1/2019
 * Time: 2:16 PM
 */

class employeeController
{

    private $db ;
    private $table = "employees" ;
    private $fillable = ['name' , 'email' , 'password' , 'code' , 'birthdate' , 'address' , 'phone' , 'image' , 'department_id' , 'salary'];

    public function __construct()
    {
        $this->db = new DB();
        $this->db->table($this->table);
        $this->db->fillable = $this->fillable;
    }
    public function index(){
        Middleware::checkAuthenticationAdmin();

        $data['title'] = "Employees";
        $data['active'] = 'Employees';
        $auth = Auth::attempt(['admin@test.com' ,'123456']) ;

        $data['employees'] = $this->db->getWithJoin('departments' ,'department_id' , 'id' );
        require __DIR__ . './../template/employees.php';
    }

    public function add(){
        Middleware::checkAuthenticationAdmin();

        $data['title'] = "Add Employee";
        $data['active'] = 'Employees';
        $data['departments'] = (new db())->table('departments')->get();
        require __DIR__ . './../template/add-employee.php';
    }

    public function store(){
        Middleware::checkAuthenticationAdmin();

        $request['name'] = $_POST['name'];
        $request['email'] = $_POST['email'];
        $request['password'] = md5($_POST['password']);
        $request['code'] = rand(999,999999);
        $request['birthdate'] = $_POST['birth'];
        $request['address'] = $_POST['address'];
        $request['phone'] = $_POST['phone'];
        $request['photo'] = uploadImage($_FILES['photo']);
        $request['department_id'] = $_POST['department'];
        $request['salary'] = $_POST['salary'];

        $validate = new Vaildatetor();
        $validate->make($request['name'])->name('name')->required();
        $validate->make($request['email'])->name('email')->required()->email();
        $validate->make($request['password'])->name('password')->required();
        $validate->make($request['birthdate'])->name('birthdate')->required();
        $validate->make($request['address'])->name('address')->required();
        $validate->make($request['phone'])->name('phone')->required();
        $validate->make($request['department_id'])->name('Department')->required();
        $validate->make($request['salary'])->name('salary')->required();

        if ($validate->fails()){
            return redirect('employees/add');
        }


        if ($this->db->insert( $this->fillable , $request)){
            return redirect('employees');
        }

    }

    public function edit($id) {
        Middleware::checkAuthenticationAdmin();

        $data['title'] = "Edit Employee";
        $data['active'] = 'Employees';
        $data['departments'] = (new DB())->table('departments')->get();
        $data['employee'] = $this->db->where('id' , $id)->getOrFail();
        require __DIR__ . './../template/edit-employee.php';
    }
    public function update($id){
        Middleware::checkAuthenticationAdmin();


        $employee = $this->db->where('id' , $id)->getOrFail();
        $request['name'] = $_POST['name'];
        $request['email'] = $_POST['email'];
        $request['code'] = $employee['code'];
        $request['password'] = $employee['password'];
        $request['birthdate'] = $_POST['birth'];
        $request['address'] = $_POST['address'];
        $request['phone'] = $_POST['phone'];

        if (!empty($_FILES['photo']['name'])){
            $request['image'] = uploadImage($_FILES['photo']);
        }else{
            $request['image'] = $employee['image'];
        }
        $request['department_id'] = $_POST['department'];
        $request['salary'] = $_POST['salary'];
        $arr = $this->fillable ;
        unset($this->fillable['password']);

        $validate = new Vaildatetor();
        $validate->make($request['name'])->name('name')->required();
        $validate->make($request['email'])->name('email')->required()->email();
        $validate->make($request['birthdate'])->name('birthdate')->required();
        $validate->make($request['address'])->name('address')->required();
        $validate->make($request['phone'])->name('phone')->required();
        $validate->make($request['department_id'])->name('Department')->required();
        $validate->make($request['salary'])->name('salary')->required();

        if ($validate->fails()){
            return redirect('employees/edit/'.$id);
        }
        if ($this->db->update($arr , $request)){
            return redirect('employees');
        }
    }

    public function delete($id){
        Middleware::checkAuthenticationAdmin();

        if ($this->db->where('id', $id)->delete()){
            return redirect('employees');
        }
    }
}