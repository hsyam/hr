<?php


class leaveController
{

    private $db ;
    private $table = "leave_requests" ;
    private $fillable = ['employee_id' , 'type' ,'description', 'date_from' , 'date_to' , 'status'];

    public function __construct()
    {
        $this->db = new DB();
        $this->db->table($this->table);
        $this->db->fillable = $this->fillable;


    }
    public function index(){
        Middleware::checkAuthenticationAdmin();
        $data['title'] = "Requests";
        $data['active'] = "Leave Requests";
        $data['leave_requests'] = $this->db->getWithJoin('employees', 'employee_id' , 'id');
        require __DIR__ . './../template/requests-list-admin.php';
    }

    public function employeeRequestsList(){
        Middleware::checkAuthenticationUser();
        $data['title'] = "Requests";
        $data['active'] = "Leave Requests";
        $data['leave_requests'] = $this->db->where('employee_id' , Auth::user()['id'])->get();
        require __DIR__ . './../template/requests-list-emp.php';

    }
    public function addRequest(){
        Middleware::checkAuthenticationUser();
        $data['title'] = "Add Request";
        $data['active'] = "Leave Requests";
        require __DIR__ . './../template/add-leave-request-emp.php';

    }
    public function submitRequest(){
        Middleware::checkAuthenticationUser();
        $user = Auth::user() ;
        $inputs['employee_id']  = $user['id'];
        $inputs['type'] = $_POST['type'];
        $inputs['description'] = $_POST['description'];
        $inputs['date_from'] = $_POST['date_from'];
        $inputs['date_to'] = $_POST['date_to'];
        $inputs['status'] = 'pending';

        $validate = new Vaildatetor();
        $validate->make($inputs['type'])->name('Request Type')->required();
        $validate->make($inputs['description'])->name('description')->required();
        $validate->make($inputs['date_from'])->name('date from')->required();
        $validate->make($inputs['date_to'])->name('date to')->required();

        if ($validate->fails()){
            return  redirect('requests/add');
        }
        if ($this->db->insert($this->fillable , $inputs)){
            return  redirect('requests');

        }

    }

    public function accept($id){
        Middleware::checkAuthenticationAdmin();
        $data = $this->db->where('id' , $id)->getOrFail();
        $data['status'] = 'accepted' ;
        if ( $this->db->update($this->fillable , $data)){
            return redirect('requestslist');
        }
    }
    public function reject($id){
        Middleware::checkAuthenticationAdmin();
        $data = $this->db->where('id' , $id)->getOrFail();
        $data['status'] = 'rejected' ;
        if ( $this->db->update($this->fillable , $data)){
            return redirect('requestslist');
        }
    }

}