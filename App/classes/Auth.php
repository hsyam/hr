<?php
/**
 * Created by PhpStorm.
 * User: engha
 * Date: 5/2/2019
 * Time: 1:13 PM
 */

class Auth
{

    public static function attempt($cred, $table = "users" , $terms = ['email' , 'password'])
    {
        $db = new DB();
        $session = new  Sessions();
        $db->table($table);
        $user = $db->where($terms[0], $cred[0])->andWhere($terms[1], md5($cred[1]))->get();
        $session->end('user');

        if ($user) {
           unset($user[0][$terms[1]]);
            $session->user = $user[0];
            return true;
        }
        return false;
    }

    public static function user(){
        return Sessions::get('user');
    }
    public static function logout(){
        Sessions::endSession('user');
    }
}