<?php
/**
 * Created by PhpStorm.
 * User: hassan
 * Date: 5/1/2019
 * Time: 3:08 PM
 *
 * Simple ORM Build by Hassan Syam
 */

class DB
{

    private $table;
    public $con;
    private $where;
    private $limit;
    private $andWhere;
    public $fillable;

    public function __construct()
    {
        $info = require __DIR__ . './../config/db.php';;

        $con = new mysqli($info['host'], $info['user'], $info['password'], $info['db_name']);

        if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);
        }

        $this->con = $con;
    }

    public function table($table){
        $this->table = $table ;
        return $this;
    }
    public function get()
    {

        if ($this->where) {
            $query = "SELECT * FROM " . $this->table . $this->where . " ORDER BY `id` DESC";
            if ($this->andWhere){
                $query = "SELECT * FROM " . $this->table . $this->where . $this->andWhere . " ORDER BY `id` DESC";
            }
        } else {
            $query = "SELECT * FROM " . $this->table . " ORDER BY `id` DESC";
        }
        if ($this->limit){
            $query .= " Limit ".$this->limit ;
        }
        $result = $this->con->query($query);
        $data = [];
        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {
                array_push($data, $row);
            }


            return $data;
        } else {
            return [];
        }
    }

    public function getOrFail()
    {
        $query = "SELECT * FROM " . $this->table . $this->where . " ORDER BY `id` DESC";
        $result = $this->con->query($query);

        if ($result->num_rows > 0) {
            return $result->fetch_assoc();
        } else {
            header('Location:' . prepareUrl() . '404');
        }
    }

    public function insert($column, $values)
    {
        $column = implode(',', $column);
        $toStringArr = array_map(function ($el) {
            return '"' . $el . '"';
        }, $values);
        $values = implode(',', $toStringArr);

        $query = "INSERT INTO " . $this->table . " (" . $column . ") VALUES " . "(" . $values . ")";

        echo $query;
        if ($this->con->query($query) === TRUE) {
            return true;
        }
        return false;
    }

    public function where($column, $value, $condation = "=")
    {
        $this->where = " Where " . $column . " " . $condation . '"' . $value .'"' ;
        return $this;
    }

    public function andWhere($column, $value, $condation = "="){
        $this->andWhere = " And  " . $column . " " . $condation . '"' . $value .'"' ;
        return $this;
    }
    public function update($column, $values)
    {
        $toStringArr = array_map(function ($el) {
            return '"' . $el . '"';
        }, $values);

        $combine = [];

        foreach ($column as $value) {

            $temp = $value . " = " . $toStringArr[$value];
            array_push($combine, $temp);
        }

        $queryStirng = implode(',', $combine);


        if ($this->where) {
            $query = "UPDATE " . $this->table . " SET " . $queryStirng . $this->where;
        } else {
            $query = "UPDATE " . $this->table . " SET " . $queryStirng;
        }

        if ($this->con->query($query) === TRUE) {
            return true;
        }
        return false;
    }

    public function delete()
    {
        $query = "DELETE FROM " . $this->table . $this->where;

        if ($this->con->query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function getWithJoin($otherTable, $table1Povit, $table2Povit = "id", $join = "INNER")
    {

        if ($this->where) {
            $query = "SELECT * , " .$this->table.".id". " FROM " . $this->table . "  " . $join . " JOIN " . $otherTable . " ON " . $otherTable . "." . $table2Povit . "=" . $this->table . "." . $table1Povit . $this->where . " ORDER BY ".$this->table.".id DESC";
        } else {
            $query = "SELECT * , " .$this->table.".id". " FROM " . $this->table . " " . $join . " JOIN " . $otherTable . " ON " . $otherTable . "." . $table2Povit . "=" . $this->table . "." . $table1Povit . " ORDER BY ".$this->table.".id DESC";
        }

        if ($this->limit){
            $query .= " Limit ".$this->limit ;
        }
        $result = $this->con->query($query);
        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $split = array_chunk($row , count($this->fillable)+1 , true) ;
                $row = $split[0] ;
                $row['relation'] = $split[1];
                array_push($data, $row);
            }

            return $data;

        } else {
            return [];
        }
    }

    public function limit($num){
        $this->limit = $num ;
        return $this;
    }
    public function fillable($fillable){
        $this->fillable = $fillable ;
        return $this;
    }
}