<?php

class Handler
{

    private $URI;
    private $routesList;


    public function __construct()
    {
        require __DIR__ . '/./../helper.php';
        $this->autoload();

        $session = new  Sessions();
        $session->start() ;

        $this->URI = $_SERVER['REQUEST_URI'];
        $this->routesList = require __DIR__ . './../config/routes.php';
        $this->runApp();
    }

    /**
     * @return mixed
     */
    private function autoload()
    {

        spl_autoload_register(function ($class_name) {
            if (file_exists(__DIR__ . '/./../Controllers/' . $class_name . '.php')) {
                include __DIR__ . '/./../Controllers/' . $class_name . '.php';
            } else if (file_exists(__DIR__ . '/./../classes/' . $class_name . '.php')) {
                include __DIR__ . '/./../classes/' . $class_name . '.php';
            }

        });
    }

    private function runApp()
    {
        $arr = explode('/', $this->URI);
        array_shift($arr);
        $arr[0] = $arr[count($arr)] = '/';
        $url = str_replace('//', '/', implode("/", $arr));

        foreach ($this->routesList as $route) {
            $pattern = $this->getPattern($route[0]);
            if (preg_match($pattern, $url)) {
                $arguments = $this->getArgumentsFrom($url, $pattern);
                if ($arguments) {

                    $controller = new $route[1];
                    $action = $route[2];
                    return $controller->$action($arguments[0]);
                }

                $controller = new $route[1];
                $action = $route[2];
                return $controller->$action();
            }
        }
        return $this->notFound();


    }

    private function getPattern($str)
    {

        $pattern = '#^';
        $pattern .= str_replace([':text', ':id'], ['([a-zA-Z0-9-]+)', '(\d+)'], $str);
        $pattern .= '/$#';
        return $pattern;
    }

    private function getArgumentsFrom($str, $pattern)
    {
        preg_match($pattern, $str, $matches);
        array_shift($matches);
        return $matches;
    }

    public function notFound()
    {
        header("HTTP/1.0 404 Not Found");
        require __DIR__ . './../template/layout/404.php';
    }
}