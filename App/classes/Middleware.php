<?php
/**
 * Created by PhpStorm.
 * User: engha
 * Date: 5/2/2019
 * Time: 2:20 PM
 */

class Middleware
{
    public static function checkAuthenticationUser()
    {
        if (!Auth::user() || isset(Auth::user()['type']) == 'admin') {
            redirect('login');
        }
    }

    public static function checkAuthentication()
    {
        if (!Auth::user()) {
            redirect('login');
        }
    }

    public static function checkAuthenticationAdmin()
    {
        if (!Auth::user() || isset(Auth::user()['type']) != 'admin') {
            redirect('login');
        }
    }

    public static function redirectIfAuthentication()
    {
        if (Auth::user()) {
            redirect('');
        }
    }

}