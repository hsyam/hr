<?php
/**
 * Created by PhpStorm.
 * User: engha
 * Date: 5/2/2019
 * Time: 1:07 PM
 */

class Sessions
{
    public function start(){
        ob_start();
        session_start();
    }

    public function __set($name, $value)
    {
        $_SESSION[$name] = $value ;
    }
    public function __get($name)
    {
        return isset($_SESSION[$name]) ?  $_SESSION[$name]: null;
    }
    public static function flash($name ,$value){
        $_SESSION[$name] = $value ;
    }
    public static function getFlash($name){
        $temp = $_SESSION[$name];
        unset($_SESSION[$name]);
        return $temp;
    }
    public static function get($name){
        return isset($_SESSION[$name]) ?  $_SESSION[$name]: null;
    }
    public function end($name){
        unset($_SESSION[$name]);
    }
    public static function endSession($name){
        unset($_SESSION[$name]);
    }
}