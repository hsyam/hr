<?php
/**
 * Created by PhpStorm.
 * User: engha
 * Date: 5/2/2019
 * Time: 1:44 AM
 */

class Vaildatetor
{
    private $name ;
    private $errors = [];
    private $fails = false ;
    private $target ;
    public function name($name){
        $this->name = $name;
        return $this;
    }

    public function make($target){
        $this->target = $target;
        return $this ;
    }
    public function required(){
        if (empty($this->target)){
            $this->fails = true ;
            $this->errors[] = 'The '.$this->name . ' is required';

            Sessions::flash('errors' , $this->errors);
        }
        return $this ;
    }
    public function email(){
        if (!filter_var($this->target,FILTER_VALIDATE_EMAIL)){
            $this->fails = true ;
            $this->errors[] = $this->name . ' is not valid Email';

            Sessions::flash('errors' , $this->errors);
        }
        return $this;
    }
    public function integer(){
        if (!filter_var($this->target , FILTER_VALIDATE_INT)){
            $this->fails = true ;
            $this->errors[] = $this->name . ' is not integer';

            Sessions::flash('errors' , $this->errors);
        }
    }
    public function errors(){
        return Sessions::getFlash('errors');
    }
    public function fails(){
        return $this->fails;
    }

}