<?php


return [
    ['/', 'HomeController', 'index'],

    ['/login', 'AuthController', 'login'],
    ['/dologin', 'AuthController', 'doLogin'],
    ['/logout', 'AuthController', 'logout'],

    ['/employees', 'employeeController', 'index'],
    ['/employees/add', 'employeeController', 'add'],
    ['/employees/store', 'employeeController', 'store'],
    ['/employees/edit/:id', 'employeeController', 'edit'],
    ['/employees/update/:id', 'employeeController', 'update'],
    ['/employees/delete/:id', 'employeeController', 'delete'],

    ['/departments', 'DepartmentsController', 'index'],
    ['/departments/add', 'DepartmentsController', 'add'],
    ['/departments/store', 'DepartmentsController', 'store'],
    ['/departments/edit/:id', 'DepartmentsController', 'edit'],
    ['/departments/update/:id', 'DepartmentsController', 'update'],
    ['/departments/delete/:id', 'DepartmentsController', 'delete'],

    ['/requests', 'leaveController', 'employeeRequestsList'],
    ['/requests/add', 'leaveController', 'addRequest'],
    ['/requests/submit', 'leaveController', 'submitRequest'],

    ['/requestslist', 'leaveController', 'index'],
    ['/requests/accept/:id', 'leaveController', 'accept'],
    ['/requests/reject/:id', 'leaveController', 'reject'],


    ['/attendance', 'AttendanceController', 'index'],
    ['/attendance/import', 'AttendanceController', 'import'],
    ['/attendance/import/submit', 'AttendanceController', 'importSubmit'],
    ['/myattendance', 'AttendanceController', 'myAttendance'],

];