<?php


function prepareUrl()
{
    $script = dirname($_SERVER['SCRIPT_NAME']);
    $requestUri = $_SERVER['REQUEST_URI'];
    if (strpos($requestUri, '?') !== false) {
        list($requestUri, $queryString) = explode('?' , $requestUri);
    }
    $url = rtrim(preg_replace('#^'.$script.'#', '' , $requestUri), '/');
    if (! $url) {
        $url = '/';
    }
    $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $script . '/';

    return $url;
}

function route($route){
    return prepareUrl() . $route ;
}

function uploadImage($file){
    $store_path = 'uploads/';
    $target_file = $store_path . basename($file["name"]);

    $type = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $newName = time().rand(1,99999). '.' . $type;
    $allowedType = ['png' , 'gif' , 'jpg' , 'jpeg'];
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        if (in_array($type , $allowedType)){
            move_uploaded_file($file["tmp_name"] , $store_path.$newName);
            return $newName;
        }

    }
}

function redirect($route){
    return header('Location:' . prepareUrl().$route);
}