<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';


?>

<section id="main-content">
    <div class="block">
        <form action="<?php echo prepareUrl() ?>departments/store" class="add-form" method="post">
            <ul>

            </ul>
            <div class="form-group">
                <?php if(Sessions::get('errors')){
                    foreach (Sessions::getFlash('errors') as $error){?>
                        <li><?php echo  $error ?></li>
                    <?php }} ?>
                <label for="Title">Title</label>
                <input type="text" name="title" id="" class="form-item">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="" cols="30" rows="10"></textarea>
            </div>


            <input type="submit" value="Add" class="btn">
        </form>
    </div>
</section>
<?php
require __DIR__ . '/layout/footer.php';
?>