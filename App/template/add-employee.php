<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

?>

    <section id="main-content">
        <div class="block">
            <form action="<?php echo route('employees/store') ?>" class="add-form" method="post" enctype="multipart/form-data">
                <?php if(Sessions::get('errors')){
                    foreach (Sessions::getFlash('errors') as $error){?>
                        <li><?php echo  $error ?></li>
                    <?php }} ?>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="" class="form-item" >
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="password">password</label>
                    <input type="password" name="password" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="birth">Birth</label>
                    <input type="date" name="birth" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="address" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="tel" name="phone" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="salary">Salary</label>
                    <input type="text" name="salary" id="" class="form-item" >
                </div>
                <div class="form-group">
                    <label for="department">Department</label>
                    <select name="department" id="" class="form-item">
                        <?php foreach ($data['departments'] as $department){ ?>
                            <option value="<?php echo $department['id']?>"><?php echo $department['title'];?></option>
                        <?php }?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="file" name="photo" id="" class="form-item" >
                </div>

                <input type="submit" value="Add" class="btn">
            </form>
        </div>
    </section>

<?php
require __DIR__ . '/layout/footer.php';
?>