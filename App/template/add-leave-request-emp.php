<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';


?>

    <section id="main-content">
        <div class="block">
            <form action="<?php echo route('requests/submit')?>" method="post" class="add-form">

                <div class="form-group">
                    <?php if(Sessions::get('errors')){
                        foreach (Sessions::getFlash('errors') as $error){?>
                            <li><?php echo  $error ?></li>
                        <?php }} ?>
                    <label for="type">Request Type</label>
                    <select name="type" id="" class="form-item">
                        <option value="annual">Annual</option>
                        <option value="permission">Permission</option>
                        <option value="sick">Sick</option>
                        <option value="vacations">Vacations</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="date_from">Date From</label>
                    <input type="datetime-local" name="date_from" id="" class="form-item" >
                </div>

                <div class="form-group">
                    <label for="date_to">Date To</label>
                    <input type="datetime-local" name="date_to" id="" class="form-item" >
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <textarea name="description" id="" cols="30" rows="10"></textarea>
                </div>


                <input type="submit" value="Submit" class="btn">
            </form>
        </div>
    </section>

<?php
require __DIR__ . '/layout/footer.php';
?>