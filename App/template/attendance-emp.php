<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

?>

    <section id="main-content">
        <div class="block">

            <div class="latest-request">
                <table class="table">
                    <thead>
                    <td>#</td>
                    <td>Month</td>
                    <td>Login</td>
                    <td>Logout</td>
                    </thead>
                    <tbody>
                    <?php foreach ($data['attendance'] as $attendance){?>
                        <tr>
                            <td><?php echo  $attendance['id']?></td>
                            <td><?php  echo  $attendance['atten_month']?></td>
                            <td><?php  echo  $attendance['login']?></td>
                            <td><?php  echo  $attendance['logout']?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php
require __DIR__ . '/layout/footer.php';
?>