<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';


?>

<section id="main-content">
    <div class="block">
        <form action="<?php echo route('attendance/import/submit') ?>" class="add-form" method="post" enctype="multipart/form-data">
            <ul>

            </ul>
            <div class="form-group">
                <?php if(Sessions::get('errors')){
                    foreach (Sessions::getFlash('errors') as $error){?>
                        <li><?php echo  $error ?></li>
                    <?php }} ?>
                <label for="atten_month">Month</label>
                <select name="atten_month" class="form-item">
                    <option value="January">January</option>
                    <option value="February">February</option>
                    <option value="March">March</option>
                    <option value="April">April</option>
                    <option value="May">May</option>
                    <option value="June">June</option>
                    <option value="July">July</option>
                    <option value="August">August</option>
                    <option value="September">September</option>
                    <option value="October">October</option>
                    <option value="November">November</option>
                    <option value="December">December</option>
                </select>
            </div>

            <div class="form-group">
                <label for="file">Description</label>
                <input type="file" name="sheet" class="form-item">
            </div>


            <input type="submit" value="Import" class="btn">
        </form>
    </div>
</section>
<?php
require __DIR__ . '/layout/footer.php';
?>