<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

?>

    <section id="main-content">
        <div class="block">
            <a  href="<?php echo prepareUrl()?>departments/add" class="btn">Add Department</a>
            <div class="latest-request">
                <table class="table">
                    <thead>
                    <td>#</td>
                    <td>Title</td>
                    <td>description</td>
                    <td>Edit</td>
                    <td>Delete</td>
                    </thead>
                    <tbody>
                    <?php foreach ($data['departments'] as $department){?>
                        <tr>
                            <td><?php echo  $department['id']?></td>
                            <td><?php  echo  $department['title']?></td>
                            <td><?php  echo  $department['description']?></td>
                            <td><a href="<?php echo prepareUrl().'departments/edit/'. $department['id']?>">Edit</a></td>
                            <td><a href="<?php echo prepareUrl().'departments/delete/'. $department['id']?>">delete</a></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php
require __DIR__ . '/layout/footer.php';
?>