<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

echo $data['title'] ;
?>

    <section id="main-content">
        <div class="block">
            <form action="<?php echo prepareUrl() ?>departments/update/<?php echo $data['department']['id']?>" class="add-form" method="post">

                <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" name="title" id="" class="form-item" required value="<?php echo $data['department']['title']?>">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="" cols="30" rows="10"><?php echo $data['department']['description']?></textarea>
                </div>


                <input type="submit" value="Save" class="btn">
            </form>
        </div>
    </section>
<?php
require __DIR__ . '/layout/footer.php';
?>