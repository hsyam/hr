<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

?>

    <section id="main-content">
        <div class="block">
            <a  href="<?php echo route('employees/add')?>" class="btn">Add Employee</a>
            <div class="latest-request">
                <table class="table">
                    <thead>
                    <td>#</td>
                    <td>Name</td>
                    <td>Department</td>
                    <td>Salary</td>
                    <td>Edit</td>
                    <td>Delete</td>
                    </thead>
                    <tbody>
                    <?php foreach ($data['employees'] as $employee){?>
                        <tr>
                            <td><?php echo  $employee['id']?></td>
                            <td><?php  echo  $employee['name']?></td>
                            <td><?php  echo  $employee['relation']['title']?></td>
                            <td><?php  echo  $employee['salary']?></td>
                            <td><a href="<?php echo prepareUrl().'employees/edit/'. $employee['id']?>">Edit</a></td>
                            <td><a href="<?php echo prepareUrl().'employees/delete/'. $employee['id']?>">delete</a></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php
require __DIR__ . '/layout/footer.php';
?>