<section id="nav-menu">
    <div class="nav-user">
            <span>
                <img src="<?php echo route('uploads/'.Auth::user()['image'])?>" alt="Hassan Syam">
                <a href="#"><?php echo Auth::user()['name']?></a>

            </span>
    </div>
    <ul>
        <?php
        if (isset(Auth::user()['type'])){
            ?>
            <li class="<?php echo ($data['active'] == 'Home') ?  "active" : "" ?>"><a href="./">Home</a></li>
            <li class="<?php echo ($data['active'] == 'Employees') ?  "active" : "" ?>"><a href="<?php echo route('employees')?>">Employees</a></li>
            <li class="<?php echo ($data['active'] == 'Departments') ?  "active" : "" ?>"><a href="<?php echo route('departments')?>">Departments</a></li>
<!--            <li class="--><?php //echo ($data['active'] == 'Reports') ?  "active" : "" ?><!--"><a href="--><?php //echo prepareUrl()?><!--requests">Reports</a></li>-->
            <li class="<?php echo ($data['active'] == 'Leave Requests') ?  "active" : "" ?>"><a href ="<?php echo route('requestslist'); ?>">Leave Requests</a></li>
            <li class="<?php echo ($data['active'] == 'Attendance') ?  "active" : "" ?>"><a href="<?php echo route('attendance');?>">Attendance</a></li>
            <li><a href="<?php echo  route('logout') ?>">Logout</a></li>
        <?php
        }else {
            ?>
            <li class="<?php echo ($data['active'] == 'Home') ?  "active" : "" ?>"><a href="./">Home</a></li>
            <li class="<?php echo ($data['active'] == 'Leave Requests') ?  "active" : "" ?>"><a href="<?php echo route('requests')?>">Leave Requests</a></li>
            <li class="<?php echo ($data['active'] == 'Attendance') ?  "active" : "" ?>"><a href="<?php echo route('myattendance') ?>">Attendance</a></li>
            <li><a href="<?php echo  route('logout') ?>">Logout</a></li>

            <?php
        }
        ?>


    </ul>
</section>
<div class="cls"></div>