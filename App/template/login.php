<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo  $data['title'] ?> | Login</title>
    <link rel="stylesheet" href="<?php echo  route('public/css/main.css')?>">
</head>
<body>

<h1 style="text-align: center" class="logo"><?php echo  $data['title'] ?></h1>
<div class="login-form">
    <form action="<?php echo route('dologin')?>" method="post" onsubmit = "return(loginValidate());" name="loginform">

        <?php if(Sessions::get('errors')){
            foreach (Sessions::getFlash('errors') as $error){?>
                <li><?php echo  $error ?></li>
            <?php }} ?>
        <div class="form-group">
            <label for="email"> Email</label>
            <input type="text" name="email" id="" class="form-item" required>
        </div>
        <div class="form-group">
            <label for="password"> password</label>
            <input type="password" name="password" id="" class="form-item" >
        </div>
        <input type="submit" value="Login" class="btn">
<!--        <a href="" class="fRight">Forget your Password</a>-->
    </form>
</div>

<script src="<?php echo route('public/js/main.js')?>">

</script>
</body>
</html>