<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';
?>

    <section id="main-content">
        <?php if (isset(Auth::user()['type'])){  ?>
        <div class="block">
            <div class="block-item">
                <span><?php echo $data['counts']['employees'] ?></span>
                <span>Employees</span>
            </div>
            <div class="block-item">
                <span><?php echo $data['counts']['departments'] ?></span>
                <span>Departments</span>

            </div>
            <div class="block-item">
                <span><?php echo $data['counts']['leave_requests'] ?></span>
                <span>Leave Requests</span>
            </div>
            <div class="block-item">
                <span><?php echo $data['counts']['attendance'] ?></span>
                <span>Attendance </span>
            </div>
        </div>

        <div class="block">
            <h3>Latest Requests</h3>
            <div class="latest-request">
                <table class="table">
                    <thead>
                    <td>#</td>
                    <td>Employee</td>
                    <td>data from</td>
                    <td>data to</td>
                    <td>Type</td>
                    <td>status</td>
                    <td>action</td>
                    </thead>
                    <tbody>
                    <?php foreach ($data['latest'] as $request){ ?>
                        <tr>
                            <td><?php echo  $request['id']?></td>
                            <td><?php  echo  $request['relation']['name']?></td>
                            <td><?php  echo  $request['date_from']?></td>
                            <td><?php  echo  $request['date_to']?></td>
                            <td><?php  echo  $request['type']?></td>
                            <td><?php  echo  $request['status']?></td>
                            <td><a href="<?php echo route('requests/accept/'.$request['id'])?>">Accept</a> , <a href="<?php echo route('requests/reject/'.$request['id'])?>">Reject</a></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <?php }else {?>
                <h3>My Requests</h3>
                <div class="latest-request">
                    <table class="table">
                        <thead>
                        <td>#</td>
                        <td>data from</td>
                        <td>data to</td>
                        <td>Type</td>
                        <td>status</td>
                        </thead>
                        <tbody>
                        <?php foreach ($data['my_requests'] as $request){ ?>
                            <tr>
                                <td><?php echo  $request['id']?></td>
                                <td><?php  echo  $request['date_from']?></td>
                                <td><?php  echo  $request['date_to']?></td>
                                <td><?php  echo  $request['type']?></td>
                                <td><?php  echo  $request['status']?></td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </section>

<?php
require __DIR__ . '/layout/footer.php';
?>