<?php
require __DIR__ . '/layout/header.php';
require __DIR__ . '/layout/nav.php';

?>

    <section id="main-content">
        <div class="block">

            <div class="latest-request">
                <table class="table">
                    <thead>
                    <td>#</td>
                    <td>Employee</td>
                    <td>data from</td>
                    <td>data to</td>
                    <td>Type</td>
                    <td>status</td>
                    <td>action</td>
                    </thead>
                    <tbody>
                    <?php foreach ($data['leave_requests'] as $leave_request){?>
                        <tr>
                            <td><?php echo  $leave_request['id']?></td>
                            <td><?php  echo  $leave_request['relation']['name']?></td>
                            <td><?php  echo  $leave_request['date_from']?></td>
                            <td><?php  echo  $leave_request['date_to']?></td>
                            <td><?php  echo  $leave_request['type']?></td>
                            <td><?php  echo  $leave_request['status']?></td>
                            <td><a href="<?php echo route('requests/accept/'.$leave_request['id'])?>">Accept</a> , <a href="<?php echo route('requests/reject/'.$leave_request['id'])?>">Reject</a></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php
require __DIR__ . '/layout/footer.php';
?>