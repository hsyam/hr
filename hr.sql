-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2019 at 05:37 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `atten_month` varchar(250) NOT NULL,
  `login` datetime NOT NULL,
  `logout` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `employee_id`, `atten_month`, `login`, `logout`) VALUES
(20, 1, 'January', '2019-05-03 07:20:00', '2019-05-03 17:10:00'),
(21, 2, 'January', '2019-05-03 08:10:00', '2019-05-03 20:23:00'),
(22, 3, 'January', '2019-05-03 10:30:00', '2019-05-03 15:18:00'),
(23, 5, 'January', '2019-05-03 09:29:00', '2019-05-03 18:30:00'),
(24, 2, 'January', '2019-05-04 08:40:00', '2019-05-04 16:29:00'),
(25, 1, 'January', '2019-05-04 10:10:00', '2019-05-04 14:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `title`, `description`) VALUES
(1, 'IT', 'it mangment'),
(3, 'Digital', 'inovation');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `birthdate` date NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `department_id` int(11) NOT NULL,
  `salary` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `password`, `code`, `birthdate`, `address`, `phone`, `image`, `department_id`, `salary`) VALUES
(1, 'hassan', 'hassan@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '198270', '1998-06-06', 'cairo', '123213412', '155689534887752.jpg', 1, '1'),
(2, 'khaled', 'admin@test.com', 'e10adc3949ba59abbe56e057f20f883e', '272038', '1990-03-03', 'Cairo', '123434523', '155675104892850.png', 1, '3'),
(3, 'saed', 'saed@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '729850', '1998-02-02', 'cair', '539959449', '155675145361657.png', 1, '4'),
(5, 'dr', 'dr@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '82552', '1990-09-09', 'egypt', '43543234', '155681322581213.png', 1, '51'),
(6, 'ahemd Ali ', 'ahmed@mail.com', '25f9e794323b453885f5181f1b624d0b', '212172', '1992-10-21', 'Cairo', '2684237482376', '155685641088856.jpg', 1, '2.5');

-- --------------------------------------------------------

--
-- Table structure for table `leave_requests`
--

CREATE TABLE `leave_requests` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `type` enum('permission','sick','annual','vacations') NOT NULL,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `status` enum('pending','accepted','rejected') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leave_requests`
--

INSERT INTO `leave_requests` (`id`, `employee_id`, `description`, `type`, `date_from`, `date_to`, `status`) VALUES
(1, 5, 'sadfasdfsdfdfsf', 'permission', '2019-06-06 09:00:00', '2019-06-09 09:00:00', 'pending'),
(2, 5, 'sdasdasfad', 'annual', '2019-11-11 11:11:00', '2019-11-18 11:11:00', 'rejected'),
(3, 1, 'safasd', 'vacations', '2019-09-01 21:09:00', '2019-09-03 21:09:00', 'accepted');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `phone` varchar(90) NOT NULL,
  `type` enum('hr','admin') NOT NULL,
  `birthdate` date NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `type`, `birthdate`, `image`) VALUES
(1, 'Admin', 'admin@test.com', 'e10adc3949ba59abbe56e057f20f883e', '3423423434', 'admin', '2019-05-15', 'avatar.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_requests`
--
ALTER TABLE `leave_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leave_requests`
--
ALTER TABLE `leave_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
